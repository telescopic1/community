# OpenHarmony community
欢迎来到OpenHarmony社区！

## 介绍
Community仓库用于管理OpenHarmony社区治理、开发者贡献指南、开发者贡献协议、社区交流等内容。

- 社区治理组织架构
    - [项目管理委员会](/zh/pmc.md)
- [开发者社区贡献指南](https://gitee.com/openharmony/docs/blob/master/zh-cn/contribute/%E8%B4%A1%E7%8C%AE%E6%8C%87%E5%8D%97.md)
- [开发者社区贡献协议](/cla)
- [社区交流](https://gitee.com/openharmony/docs/blob/master/zh-cn/contribute/%E7%A4%BE%E5%8C%BA%E6%B2%9F%E9%80%9A%E4%B8%8E%E4%BA%A4%E6%B5%81.md)
- [安全问题响应](https://gitee.com/openharmony/security)
- [项目LOGO](/logo)

## 社区治理组织架构

OpenHarmony社区通过[项目管理委员会](/zh/pmc.md)（ Project Management Committee）管理OpenHarmony社区。

## 开发者社区贡献指南

请阅读[如何贡献](https://gitee.com/openharmony/docs/blob/master/zh-cn/contribute/%E5%8F%82%E4%B8%8E%E8%B4%A1%E7%8C%AE.md)获得帮助。

## 开发者社区贡献协议

请使用[CLA](/cla)模板签署CLA贡献协议，并发送协议到contact@openharmony.io邮箱。

## 社区交流

### OpenHarmony maillist 交流方式
| 地址                                 | 简介        | 用途说明                                                         |
| ---------------------------------------|---------- | ------------------------------------------------------------ |
| contact@openharmony.io <img width=150/>  | 公用邮箱 <img width=100/> | OpenHarmony社区公共邮箱。开发者CLA协议签署可以发邮件到此邮箱。<img width=200/>|
| dev@openharmony.io  <img width=150/>| 开发邮件列表 <img width=100/> | OpenHarmony社区开发讨论邮件列表，任何社区开发相关话题都可以在邮件列表讨论。任何开发者可[订阅](https://lists.openatom.io/postorius/lists/dev.openharmony.io)。<img width=200/>|
| cicd@openharmony.io <img width=150/> | CI邮件列表  <img width=100/>| OpenHarmomny CICD构建邮件列表，任何开发者可[订阅](https://lists.openatom.io/postorius/lists/cicd.openharmony.io)。<img width=200/>|
| pmc@openharmony.io  <img width=150/>| PMC邮件列表  <img width=100/>| PMC讨论邮件列表，PMC成员可[订阅](https://lists.openatom.io/postorius/lists/pmc.openharmony.io/)。<img width=200/>|
| scy@openharmony.io <img width=150/> | 安全问题邮箱 <img width=100/> | 开发者可反馈OpenHarmony安全问题到此邮箱。<img width=200/>|
| scy-priv@openharmony.io  <img width=150/>| 安全组邮件列表  <img width=100/>| 安全组成员安全问题处理讨论邮件列表，安全组成员可[订阅](https://lists.openatom.io/postorius/lists/scy-priv.openharmony.io/)。<img width=200/>|


### OpenHarmony Slack 交流频道
| 技术交流平台                                 | 简介        | 用途说明                                                         |
| ---------------------------------------|---------- | ------------------------------------------------------------ |
| [Slack](https://join.slack.com/t/openharmonyworkspace/shared_invite/zt-p4xc67eq-nQqrjq7yyNDG85ptGNIZTQ) <img width=150/>  | OpenHarmony 技术开放讨论平台 <img width=100/> | OpenHarmony的Slack 主要用于内核技术相关问题讨论。<img width=200/>|


## 项目LOGO

请从[OpenHarmony LOGO](/logo)下载并使用LOGO。
