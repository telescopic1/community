# SIG Template
English | [简体中文](./sig_devboard_cn.md)

Note: The content of this SIG follows the convention described in OpenHarmony's PMC Management Charter [README](/zh/pmc.md).

## SIG group work objectives and scope


To increase the number of third-party boards of the OpenHarmony OS, the device SIG 
provides easy porting guide that board manufacturers could follow to contribute 
their codes to the community easily. The more important thing is actively cooperating 
with board manufacturers to enhance the OpenHarmony ecosystem.

### Scope

- Easy Porting Requirements

Proposes easy porting requirements that all OpenHarmony projects, not only kernel and building project, should 
develop.

- Porting and Contributing Guidelines

Guides board manufacturers to contribute their codes to the community.

- Demo Projects

Creates demo projects, hi3516/hi3518/hi3861, for others to follow.

- Expanding Ecosystems

Actively cooperates with board manufacturers.

### The repository 
- project name:
  - device_st: https://gitee.com/openharmony-sig/device_st
  - vendor_huawei_minidisplay_demo: https://gitee.com/openharmony-sig/vendor_huawei_minidisplay_demo


## SIG Members

### Leader
- [SimonLi](https://gitee.com/kkup180)

### Committers
- [zianed](https://gitee.com/zianed)
- [DennyShen](https://gitee.com/DennyShen)
- [borne](https://gitee.com/borne)
- [jady3356](https://gitee.com/taiyipei)

### Meetings
 - Meeting time：BiWeek Monday 19:00
 - Meeting link：slack qemu channel

### Contact (optional)

- Mailing list：xxx
- Slack group：[qemu](https://openharmonyworkspace.slack.com/archives/C01G1DEHLR5)
- Wechat group：xxx
